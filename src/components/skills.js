export default function Skills() {
  return (
    <section className="section">
      <div className="container">
        <h1 className="title">My skills</h1>
        <div className="tile is-ancestor">
          <div className="tile is-vertical is-8">
            <div className="tile">
              <div className="tile is-parent is-vertical">
                <article className="tile is-child notification is-primary">
                  <p className="title">Gitlab CI/CD</p>
                  <p className="subtitle">
                    I made a lot of automated stuff with gitlab !
                  </p>
                </article>
                <article className="tile is-child notification is-warning">
                  <p className="title">Kubernetes</p>
                  <p className="subtitle">
                    Made some scalable application thanks to K8S.
                  </p>
                </article>
              </div>
              <div className="tile is-parent">
                <article className="tile is-child notification is-link">
                  <p className="title">Docker</p>
                  <p className="subtitle">
                    Containerization is the future, I practice a lot Docker and
                    build some great images for my application !
                  </p>
                  <figure className="image">
                    <img src="./assets/docker.png" alt="Docker logo" />
                  </figure>
                </article>
              </div>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child notification is-danger">
                <p className="title">CleverCloud</p>
                <p className="subtitle">
                  All my applciation are hosted on
                  <a
                    href="https://www.clever-cloud.com/fr/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    CleverCLoud
                  </a>
                  ! <br />
                  What an amazing platform !
                </p>
                <div className="content"></div>
              </article>
            </div>
          </div>
          <div className="tile is-parent">
            <article className="tile is-child notification is-info">
              <div className="content">
                <p className="title">React</p>
                <p className="subtitle">
                  I love React, I made some cool stuff with it !
                </p>
                <div className="content"></div>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>
  );
}
